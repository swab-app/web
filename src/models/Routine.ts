import RoutineExercise from './RoutineExercise';

interface Routine {
    name: string,
    id: number
    routineExercises: RoutineExercise[]
}

export default Routine;