// Set is reserved for typescript, wonder what I can do
export default interface SetModel {
    weight: number, 
    distance: number,
    time: number,
    type: string,
    reps: number
}