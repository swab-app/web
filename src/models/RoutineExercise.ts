import Exercise from './Exercise';
import SetModel from './SetModel';

interface RoutineExercise {
    order: number
    exercise: Exercise
    sets: SetModel[]
}

export default RoutineExercise;