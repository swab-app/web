export interface SetRequest {
    weight: number,
    time: number, 
    reps: number,
    type: string,
    distance: number
}

export interface RoutineExerciseRequest {
    id: number,
    sets: SetRequest[]
}

export interface RoutineRequest {
    name: string,
    exercises: RoutineExerciseRequest[],
}

export interface WorkoutExerciseRequest {
    id: number,
    sets: SetRequest[]
}

export interface WorkoutRequest {
    name: string,
    exercises: WorkoutExerciseRequest[]
}