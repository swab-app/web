import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import * as colours from './colours';
// import * as variables from './variables';

// Common Html elements.
const H1 = styled.h1`

`;

const H2 = styled.h2`
    
`;

const buttonStyles = css`
    background: ${colours.primary};
    color: ${colours.textOnPrimary};
    border-radius: 4px;
    padding: 8px 16px;
    border: none;
    text-decoration: none;
    &:hover, &:active {
        background: ${colours.primaryHover}
    }
`;

const Button = styled.button`${buttonStyles}`;
const A = styled.a`${buttonStyles}`;
const RouterLink = styled(Link)`${buttonStyles}`;

export { H1, H2, Button, A, RouterLink }