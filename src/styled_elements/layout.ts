import styled from 'styled-components';
import * as colours from './colours';
import * as variables from './variables';

const Background = styled.div`
    background: ${colours.background};
    font-family: 'Inter', sans-serif;
    width: 100%;
    min-height: 100vh;
    height: 100%;
    display:flex;
    flex-direction: column;
    justify-content: top;
`;

const MainContentWrapper = styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
`;

const MainContentBox = styled.div`
    width: ${variables.largeContent.mainWidth};
    background: ${colours.panelBackground};
    min-height: 64vh;
    margin: 32px 0px 32px 0;
    border-radius: 10px;
`;

const MainContent = styled.div`
    margin: 0px ${variables.largeContent.gutterWidth} 0px ${variables.largeContent.gutterWidth};
`;

const FormRow = styled.div`
    display:flex;
    flex-direction: column;
    justify-content: flex-start;
    label {
        padding-bottom: 8px;
    }
    input {
        max-width: 240px;
    }
    padding-bottom: 16px;
`
const ButtonRow = styled.div`
    display:flex;
    flex-direction: row;
    justify-content: flex-end;
`

const FlexBetweenRow = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export { Background, MainContent, MainContentBox, MainContentWrapper, FormRow, ButtonRow, FlexBetweenRow};