// Colours 

// this is a nice green: #1BC18A

const background = "#EEE";
const primary = '#3B4B21';
const primaryHover = "#6E7E54";
const textOnPrimary = '#FFF';
const panelBackground = "#FFF";

export { background, primary, primaryHover, textOnPrimary, panelBackground };