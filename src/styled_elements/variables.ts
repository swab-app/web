const largeContent = {
    mainWidth: "1248px",
    gutterWidth: "64px"
}

export { largeContent };