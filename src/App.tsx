import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch
} from 'react-router-dom';
import { User } from './models';
import { Authenticator, AuthUserContext } from './utilities';
import { Navigation } from './components/Navigation';
import { Workout } from './components/Workout';
import { Login, Register, NotFound } from './components';
import { CreateRoutine, EditRoutine } from './components/Routine';
import { Home } from './components/Home';
import { Background, H1, MainContent, MainContentBox, MainContentWrapper } from './styled_elements';

interface AppState {
  user: User | null,
  appName: string
}

class App extends React.Component<any, AppState>{
  constructor(props: any) {
    super(props)
    this.state = {
      user: Authenticator.getUser(),
      appName: process.env.REACT_APP_NAME ?? "SWAB"
    }
  }

  onLogin(user: User) {
    Authenticator.setUser(user);
    this.setState({ user: user });
  }

  logOut() {
    this.setState({ user: null }, () => Authenticator.removeUser());
  }

  render() {
    const { user, appName } = this.state;
    return (
      <AuthUserContext.Provider value={user}>
        <Background>
          <Router>
            <Navigation logoutCallback={() => this.logOut()} name={appName} />
            <MainContentWrapper>
              <MainContentBox>
                <MainContent>
                  <Switch>
                    <Route path='/' exact>
                      {user === null ? <h1>SWAB</h1> : <Redirect to="/home" />}
                    </Route>
                    <Route path='/login'>
                      <Login callback={user => this.onLogin(user)} />
                    </Route>
                    <Route path='/register'>
                      <Register callback={user => this.onLogin(user)} />
                    </Route>
                    <Route path='/home'>
                      {user !== null ? <Home user={user}/> : <Redirect to='/' />}
                    </Route>
                    <Route path='/routine/create'>
                      {user !== null ? <CreateRoutine /> : <Redirect to='/' />}
                    </Route>
                    <Route path="/routine/edit/:id" component={EditRoutine} />
                    <Route path="/workout/:id?" component={Workout} exact />
                    <Route path="*" component={NotFound} />
                  </Switch>
                </MainContent>
              </MainContentBox>
            </MainContentWrapper>
          </Router>
        </Background>
      </AuthUserContext.Provider>
    );
  }
}

export default App;
