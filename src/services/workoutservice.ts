import { WorkoutRequest } from "../requests";
import { http } from "../utilities";

class WorkoutService {
    static create(workout: WorkoutRequest, callback: () => void) : void {
        http.post("/api/workout", workout)
            .then(response => {
                console.log(response.data);
                callback();
            })
            .catch(err => {
                console.error(err);
            });
    }
}

export default WorkoutService;