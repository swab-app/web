import { http } from "../utilities";
import { RoutineRequest } from '../requests';
import { Routine } from "../models";
import { AxiosResponse } from "axios";

class RoutineService {
    static getRoutineById(id: number) : Promise<AxiosResponse<Routine>> {
        return http.get(`/api/routine/${id}`);
    }

    static create(routine: RoutineRequest, callback: () => void) : void {
        http.post('/api/routine', routine)
            .then(response => {
                console.log(response.data);
                callback();
            })
            .catch(err => {
                console.error(err);
            })
    }

    static update(id: number, routine: RoutineRequest, callback: () => void) : void {
        http.post(`/api/routine/${id}`, routine)
            .then(response => {
                console.log(response.data);
                callback();
            })
            .catch(err => {
                console.error(err);
            })
    }

}

export { RoutineService }