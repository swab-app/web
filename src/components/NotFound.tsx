import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

class NotFound extends React.Component<RouteComponentProps> {
    render() {
        return <h1>
        No route found for {this.props.location.pathname}
      </h1>
    }
}

export default NotFound;