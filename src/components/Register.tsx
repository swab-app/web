import React from 'react'
import { Redirect } from 'react-router-dom';
import { Authenticator, http } from '../utilities';
import { User } from '../models';

interface RegisterState {
    username: string,
    email: string,
    password: string
    passwordConf: string
    message: string
}


interface RegisterProps {
    callback: (user: User) => void
}

interface RegisterResponse {
    User: User,
    token: string
}

class Register extends React.Component<RegisterProps,RegisterState> {

    onInputChange(key:string, e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({[key]: e.target.value} as Pick<RegisterState, keyof RegisterState>);
    }

    register() {
        console.log("We want to register:");
        console.log(this.state);
        const { passwordConf, message, ...request } = this.state;
        http.post<RegisterResponse>('/register', request)
            .then(response => {
                let user: User = {
                    username: response.data.User.username,
                    id: response.data.User.id,
                    email: response.data.User.email,
                    token: response.data.token
                }
                this.props.callback(user);
            })
            .catch(error => {
                console.error(error);
            })
    }

    render() {
        return Authenticator.isLoggedIn() 
        ? <Redirect to="/home"/>
        : <div>
            <h1>Register</h1>
            <div className="form-row">
                <label htmlFor="username">Username</label>
                <input type="text" name="username" onChange={e => this.onInputChange('username', e)}/>
            </div>
            <div className="form-row">
                <label htmlFor="email">Email</label>
                <input type="text" name="email" onChange={e => this.onInputChange('email', e)}/>
            </div>
            <div>
                <label htmlFor="password">Password</label>
                <input type="password" name="password" onChange={e => this.onInputChange('password', e)}/>
            </div>
            <div>
                <label htmlFor="passwordConf">Confirm password</label>
                <input type="password" name="passwordConf" onChange={e => this.onInputChange('passwordConf', e)}/>
            </div>
            <div className="form-row">
                <input type="button" value="Register" onClick={() => this.register()}/>
            </div>
        </div>
    }
}

export default Register;