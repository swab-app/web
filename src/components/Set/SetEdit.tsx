import React from 'react';
import {SetModel} from '../../models';

interface SetState {
    weight: number
    reps: number
}

interface Props {
    set: SetModel,
    callback: (set: SetModel) => void
}

class SetEdit extends React.Component<Props, SetState> {
    constructor(props:Props) {
        super(props);
        this.state = {
            weight: this.props.set.weight,
            reps: this.props.set.reps
        };
    }

    updateSet(weight: number, reps: number) {
        this.setState({weight: weight, reps: reps}, () => {
            const {weight, reps} = this.state; 
            const set:SetModel = {
                weight: weight,
                reps: reps,
                distance: 0,
                time: 0,
                type: ''
            };
            this.props.callback(set);
        });
    }
    
    render() {
        const {weight, reps} = this.state;
        return <>
            <input type="number" name="weight" value={weight} placeholder="weight"
                onChange={e => this.updateSet(parseFloat(e.target.value), reps)} />
            <input type="number" name="reps" value={reps} placeholder="reps"
                onChange={e => this.updateSet(weight, parseFloat(e.target.value))} />
        </>
    }
}

export default SetEdit;