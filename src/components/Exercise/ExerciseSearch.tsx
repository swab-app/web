import React from 'react';
import { Exercise } from '../../models';
import { Button } from '../../styled_elements';
import { http } from '../../utilities';
import {SearchInputRow, SearchWrapper, SearchResults, SearchTextInput, SearchSelect, SearchResult} from './styles';


// You're trying to style it like thus:
// [ <icon> Search Term                           [Type v] [button]]
// [ Result A ]                                   [Type  ]
// [ Result B ]                                   [Type  ]
// [ Result C ]                                   [Type  ]

interface ExerciseSearchState {
    name: string,
    type: string,
    exercises: Exercise[],
}

interface ExerciseRequest {
    name: string,
    type: string
}


interface ExerciseSearchProps {
    selectedCallback: (exercise:Exercise) => void
}


class ExerciseSearch extends React.Component<ExerciseSearchProps,ExerciseSearchState> {
    constructor(props:ExerciseSearchProps) {
        super(props);
        this.state = {
            name: '',
            type: '',
            exercises: []
        }
    }
    componentDidMount() {
        this.search();
    }

    search() {
        const exerciseRequest: ExerciseRequest = {
            name: this.state.name,
            type: this.state.type
        };
        http.get("/api/exercise", {params: exerciseRequest})
            .then(response => {
                this.setState({exercises: response.data});
            })
            .catch(error => {
                console.error(error);
            });      
    }

    exerciseSelected(ex:Exercise) {
        this.props.selectedCallback(ex);
    }

    render() {
        const exerciseList = this.state.exercises.map((exercise:Exercise) => {
            return <SearchResult key={exercise.id} className="ex-button" onClick={() => this.exerciseSelected(exercise)}>
                {exercise.name}
            </SearchResult>
        });

        return <SearchWrapper>
            <SearchInputRow>
                <SearchTextInput type="text" name="name" placeholder="Exercise Name" onChange={e => {
                        this.setState({name: e.target.value});
                        console.log("state changed");
                    }
                }/>
                <SearchSelect onChange={e => this.setState({type: e.target.value})} >
                    <option value="REPS">Reps</option>
                    <option value="TIMED">Timed</option>
                    <option value="DISTANCE">Distance</option>
                </SearchSelect>
                <Button onClick={() => this.search()}>Search</Button>
            </SearchInputRow>
            <SearchResults>
                {exerciseList}
            </SearchResults>
        </SearchWrapper>
    }
}

export default ExerciseSearch;