import styled from 'styled-components';
import * as colours from '../../styled_elements';

const SearchInputRow = styled.div`
    display:flex;
    flex-direction:row;
    align-items:center;
    justify-content: flex-start;
    border-bottom: 1px solid ${colours.primary};
    height: 48px;
`;

const SearchWrapper = styled.div`
    position: static;
`;
const SearchResults = styled.div`
    display: flex;
    flex-direction: column;
`;

const SearchTextInput = styled.input`
    border: 0;
    outline:none;
`;

const SearchSelect = styled.select`
    background: transparent;
    border: 0;
`

const SearchResult = styled.button`
    padding: 8px 16px;
    background: transparent;

    border: 1px solid ${colours.primary};
    border-top: 0;

    &:hover { 
        background: ${colours.primary};
        color:white;
    }
`;

export {SearchInputRow, SearchWrapper, SearchResults, SearchTextInput, SearchSelect, SearchResult};