import React from 'react'
import { RouteComponentProps } from 'react-router-dom';
import { Exercise, Routine, RoutineExercise, SetModel } from '../../models';
import { RoutineExercise as WorkoutExerciseComponent } from '../RoutineExercise';
import { RoutineService } from '../../services';
import { Button, ButtonRow, FlexBetweenRow, FormRow, H1, H2 } from '../../styled_elements';
import { isNotNullOrUndefined, isNullOrUndefined } from '../../utilities';
import { ExerciseSearch } from '../Exercise';

interface RouteParameters {
    id: string,
}

interface WorkoutProps extends RouteComponentProps<RouteParameters> {}

interface WorkoutState {
    name: string,
    exercises: Exercise[]
}

class Workout extends React.Component<WorkoutProps, WorkoutState> {
    constructor(props: WorkoutProps) {
        super(props)
        this.state = {
            name: '',
            exercises: []
        }
    }

    componentDidMount() {
        const searchParams = new URLSearchParams(this.props.location.search)
        const routineId = searchParams.get("routineId");
        const workoutId = this.props.match.params.id;

        
        if (isNotNullOrUndefined(workoutId)) {
            console.log("need to go get a workout to populate form ...", workoutId);
            //this.retrieveWorkout(parseInt(workoutId!));
        } else if (isNotNullOrUndefined(routineId)) {
            console.log("need to go fetch routine exercises ...", routineId);
            this.retrieveRoutine(parseInt(routineId!));
        }
    }

    retrieveRoutine(routineId: number) {
        RoutineService.getRoutineById(routineId)
            .then(response => {this.setState(this.transformRoutineIntoState(response.data))})
            .catch(err => console.error(err));
    }

    retrieveWorkout() {
        // do something :)
    }

    create() {
        // lol 
    }

    update() {
        // lol
    }

    addExercise(ex:Exercise) {
        let {exercises} = this.state;
        exercises.push({
            id: ex.id,
            name: ex.name,
            type: ex.type,
            sets: ex.sets,
        });
        this.setState({exercises: exercises});
    }

    removeExercise(index:number) {
        let {exercises} = this.state;
        exercises.splice(index, 1);
        this.setState({exercises: exercises});
    }

    updateExerciseSets(index:number, sets:SetModel[]) {
        let {exercises} = this.state;
        exercises[index].sets = sets;
        this.setState({exercises: exercises});
    }

    // todo: replace this with some kind of DTO / mapping service.
    transformRoutineIntoState(r: Routine) : WorkoutState {
        return {
            name: r.name,
            exercises: r.routineExercises.map((re:RoutineExercise) => {
                let ex: Exercise = re.exercise
                ex.sets = re.sets;
                return ex;
            })
        };
    }

    renderExerciseList() {
        // ToDo: replace this with a 'WorkoutExerciseList'
        //       may be a PITA because I'm passing callbacks down the chain. 
        return this.state.exercises.length === 0
            ? <p>No exercises in workout</p>
            : this.state.exercises.map((ex:Exercise, index:number) => {
                return <WorkoutExerciseComponent
                    key={`${ex.name}-${index}`}
                    exercise={ex}
                    removeCallback={() => this.removeExercise(index)}
                    updateCallback={(sets:SetModel[]) => this.updateExerciseSets(index, sets)}
                    />
            });
    }

    render() {
        const isCreate = isNullOrUndefined(this.props.match.params.id);
        const {name} = this.state;
        const exerciseList = this.renderExerciseList()
        return <div>
            <H2>{name} Workout</H2>
            <FormRow>
                <label htmlFor="name">Workout Name:</label>
                <input type="text" name="name" value={name} onChange={e => this.setState({name: e.target.value})} />
            </FormRow>
            <FlexBetweenRow>
                <ExerciseSearch selectedCallback={(ex:Exercise) => this.addExercise(ex)} />
                <div style={{minWidth: "50%"}}>
                    {exerciseList}
                </div>
            </FlexBetweenRow>
            <ButtonRow>
                <Button onClick={() => isCreate ? this.create() : this.update()}>
                    Save Workout
                </Button>
            </ButtonRow>
        </div>;
    }
}

export default Workout;