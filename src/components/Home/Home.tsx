import React from 'react';
import styled from 'styled-components';
import { http } from '../../utilities';
import { User, Routine, RoutineExercise } from '../../models';
import { RouterLink as Link } from '../../styled_elements';

interface HomeProps {
    user: User,
}

interface HomeState {
    routines: Routine[]
}

const RoutineListWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
`;

const RoutineListItem = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    padding: 16px;
    min-width: 29%;
    margin: 0px 8px 32px 0px;
    height: 250px;
    box-shadow: 5px 5px 15px lightgray;
    border-radius: 15px;
`;

const ButtonRow = styled.div`
    text-align: right;
    a:first-child {
        margin-right: 8px;
    }
`;

class Home extends React.Component<HomeProps,HomeState> {
    constructor(props:HomeProps) {
        super(props);
        this.state = {
            routines: []
        };
    }

    getRoutines() {
        http.get("/api/routine")
            .then(response => {
                console.log(response.data[0]);
                this.setState({routines: response.data});
            })
            .catch(error => {
                console.error(error);
            })
    }

    componentDidMount() {
        this.getRoutines();
    }

    routineList(routine:Routine) {
        const numberOfExercises = routine.routineExercises.length;
        let exerciseList:string = routine.routineExercises
            .slice(0, 2)
            .map((routineEx:RoutineExercise) => {
                return routineEx.exercise.name
            })
            .join(", ");
        if (numberOfExercises > 3) {
            exerciseList += " and more.";
        }

        return <RoutineListItem>
            <div>
                <p><strong>{routine.name}</strong></p>
                <p>{routine.routineExercises.length} {numberOfExercises > 1 ? "exercises" : "exercise"}</p>
                <p>{exerciseList}</p>
            </div>
            <ButtonRow>
                <Link to={`/routine/edit/${routine.id}`}>Edit</Link>
                <Link to={`/workout?routineId=${routine.id}`}>Start</Link>
            </ButtonRow>
        </RoutineListItem>;
    }

    render() {
        const { user } = this.props;
        const routineList = this.state.routines.map(this.routineList);
        return <div>
            <h1>Hello, {user.username}</h1>
            <RoutineListWrapper>
                {routineList}
            </RoutineListWrapper>
        </div>;
    }
}

export default Home;