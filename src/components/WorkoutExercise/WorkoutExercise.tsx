import { render } from '@testing-library/react';
import React from 'react';
import styled from 'styled-components';
import { Exercise, SetModel } from '../../models';
import { FlexBetweenRow } from '../../styled_elements';
import { isNotNullOrUndefined } from '../../utilities';

const WorkoutControlRow =  styled(FlexBetweenRow as any)`
    border-bottom: 1px solid #3B4B21;
    align-items: center;
`;

interface State {
    sets: SetModel[],
}

interface Props {
    exercise: Exercise
    removeCallback: () => void,
    updateCallback: (sets:SetModel[]) => void,
}

class WorkoutExercise extends React.Component<Props, State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            sets: []
        };
    }

    componentDidMount() {
        let {exercise} = this.props;
        if (isNotNullOrUndefined(exercise) && isNotNullOrUndefined(exercise.sets)) {
            this.setState({sets: exercise!.sets!});
        }
    }

    addSet() {
        let {sets} = this.state;
        sets.push({weight: 0, distance: 0, time: 0, reps:0, type: this.props.exercise.type});
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    removeSet(selectedIndex:number) {
        let {sets} = this.state;
        sets.splice(selectedIndex, 1);
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    updateSet(set: SetModel, index: number) {
        let {sets} = this.state;
        sets[index] = set;
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    render() {
        return <p>hello lol</p>
    }
}

export default WorkoutExercise;