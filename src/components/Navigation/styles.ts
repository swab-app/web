import styled from 'styled-components';
import { Link } from 'react-router-dom';
import * as colours from '../../styled_elements/colours';
import * as variables from '../../styled_elements/variables';

const NavigationOuterWrapper = styled.div`
    height: 64px;
    background: ${colours.primary};
    color: ${colours.textOnPrimary};
    display: flex;
    justify-content: center;
`

const NavigationInnerWrapper = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: ${variables.largeContent.mainWidth};
    
    h1 { 
        margin: 0;
        text-transform: uppercase;
        font-weight: normal;
    }

    div.nav-left {
        margin-left: ${variables.largeContent.gutterWidth};
        height: 100%;
        display: flex;
        align-items: center;
    }

    div.nav-right {
        margin-right: ${variables.largeContent.gutterWidth};
        height: 100%;
        display: flex;
        align-items: center;
        font-size: 14px;
    }

    * {
        color: white;
        font-weight: normal;
        text-decoration: none;
    }
`;

const NavLink = styled.a`
    min-width: 120px;
    height: 100%;
    background: transparent;
    border: none;s
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover, &:active { 
        background: ${colours.primaryHover};
        border-bottom: 2px solid white;
    }
`

const NavRouterLink = styled(Link)`
    min-width: 120px;
    height: 100%;
    background: transparent;
    border: none;
    display: flex;
    align-items: center;
    justify-content: center;
    &:hover, &:active { 
        background: ${colours.primaryHover};
        border-bottom: 2px solid white;
    }
`

export { NavRouterLink, NavLink, NavigationOuterWrapper, NavigationInnerWrapper };