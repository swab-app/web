import React from 'react';
import {NavigationOuterWrapper, NavigationInnerWrapper, NavRouterLink } from './styles';
import {H1} from '../../styled_elements';

interface NavigationPubProps {
    name: string
}

class NavigationPublic extends React.Component<NavigationPubProps,any> {
    render () {
        const {name} = this.props;
        return <NavigationOuterWrapper>
            <NavigationInnerWrapper>
                <div className="nav-left">
                    <H1>{name}</H1>
                </div>
                <div className="nav-right">
                    <NavRouterLink to='/login'>Login</NavRouterLink>
                    <NavRouterLink to='/register'>Register</NavRouterLink>
                </div>
           </NavigationInnerWrapper>
        </NavigationOuterWrapper>;
    }
}

export default NavigationPublic;