import React from 'react';
// import { H1, NavigationOuterWrapper, NavigationInnerWrapper, NavLink, NavRouterLink } from './styles';
import {NavigationOuterWrapper, NavigationInnerWrapper, NavLink, NavRouterLink} from './styles';
import {H1} from '../../styled_elements';
import { Link } from 'react-router-dom';

interface NavigationAuthProps {
    logoutCallback: () => void,
    name: string
}

class NavigationAuth extends React.Component<NavigationAuthProps, any> {
    render () {
        const {name, logoutCallback} = this.props;
        return <NavigationOuterWrapper>
            <NavigationInnerWrapper>
                <div className="nav-left">
                    <H1><Link to="/home">{name}</Link></H1>
                </div>
                <div className="nav-right">
                    <NavRouterLink to='/routine/create'>New Routine</NavRouterLink>
                    <NavLink as="button" onClick={() => logoutCallback()}>Log out :-)</NavLink>
                </div>
            </NavigationInnerWrapper>
        </NavigationOuterWrapper>
    }
}

export default NavigationAuth;