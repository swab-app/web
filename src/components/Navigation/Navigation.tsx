import React from 'react';
import {AuthUserContext} from '../../utilities';
import NavigationAuth from './NavigationAuth';
import NavigationPublic from './NavigationPublic'

interface NavigationProps {
    logoutCallback: () => void,
    name: string
}

class Navigation extends React.Component<NavigationProps, {}> {
    static contextType = AuthUserContext;
    // can now log in, next steps navigation + logout button.

    render() {
        const  user  = this.context;
        const { name, logoutCallback } = this.props;
        if (user != null) {
            return <NavigationAuth logoutCallback={logoutCallback} name={name}/>
        } else {
            return <NavigationPublic name={name}/>
        }
    }
}

export default Navigation;