import React from 'react';
import { Exercise, SetModel } from '../../models';
import SetEdit from '../Set/SetEdit';
import { FlexBetweenRow } from '../../styled_elements';
import styled from 'styled-components';
import { isNotNullOrUndefined } from '../../utilities';

const RoutineControlRow =  styled(FlexBetweenRow as any)`
    border-bottom: 1px solid #3B4B21;
    align-items: center;
`;

interface State {
    sets: SetModel[],
}

interface Props {
    exercise: Exercise
    removeCallback: () => void,
    updateCallback: (sets:SetModel[]) => void,
}

class RoutineExercise extends React.Component<Props,State> {
    constructor(props:Props) {
        super(props);
        this.state = {
            sets: []
        };
    }

    componentDidMount() {
        let {exercise} = this.props;
        if (isNotNullOrUndefined(exercise) && isNotNullOrUndefined(exercise.sets)) {
            this.setState({sets: exercise!.sets!});
        }
    }

    addSet() {
        let {sets} = this.state;
        sets.push({weight: 0, distance: 0, time: 0, reps:0, type: this.props.exercise.type});
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    removeSet(selectedIndex:number) {
        let {sets} = this.state;
        sets.splice(selectedIndex, 1);
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    updateSet(set: SetModel, index: number) {
        let {sets} = this.state;
        sets[index] = set;
        this.setState({sets: sets}, () => this.props.updateCallback(sets));
    }

    render(){
        const {sets} = this.state;
        const {removeCallback} = this.props
        const setList = sets.map((set:SetModel, index:number) => {
            const key = `set-${this.props.exercise.name}-${index}`
            return <div key={key}>
                <SetEdit set={set} key={key} callback={(set:SetModel) => this.updateSet(set, index)} />
                <button onClick={() => this.removeSet(index)}>-</button>
            </div>;
        });
        return <>
            <RoutineControlRow>
                <p>{this.props.exercise.name}</p>
                <div>
                    <button onClick={() => this.addSet()}>+</button>
                    <button onClick={() => this.removeSet(this.setState.length -1)}>-</button>
                    <button onClick={() => removeCallback()}>Remove</button>
                </div>
            </RoutineControlRow>
            {setList}
        </>
    }
}

export default RoutineExercise;