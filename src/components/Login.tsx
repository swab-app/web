import React from 'react';
import { Redirect } from 'react-router-dom';
import { Authenticator, http } from '../utilities';
import { User } from '../models';

interface LoginState {
    username: string
    password: string
    message: string
}
interface LoginResponse {
    User: User,
    token: string
}

interface LoginProps {
    callback: (user: User) => void
}
class Login extends React.Component<LoginProps, LoginState> {
    constructor(props:LoginProps) {
        super(props);
        this.state = { username: '', password: '', message: '' }
    }

    onInputChange(key:string, e: React.ChangeEvent<HTMLInputElement>) {
        this.setState({[key]: e.target.value} as Pick<LoginState, keyof LoginState>);
    }

    login() {
        const { message, ...request } = this.state;
        http.post<LoginResponse>("/login", request)
        .then(response => {
            let user: User = {
                username: response.data.User.username,
                id: response.data.User.id,
                email: response.data.User.email,
                token: response.data.token
            }
            this.props.callback(user);
        })
        .catch(error => {
            console.error(error);
        })
    }

    render() {
        return Authenticator.isLoggedIn() 
            ? <Redirect to='/home' />
            : <div>    
                <h1>login :)</h1>
                <div className="form-row">
                    <label htmlFor="username">Username</label>
                    <input type="text" name="username" onChange={e => this.onInputChange('username', e)}/>
                </div>
                <div className="form-row">
                    <label htmlFor="username">Password</label>
                    <input type="password" name="password" onChange={e => this.onInputChange('password', e)}/>
                </div>
                <div className="form-row">
                    <input type="button" value="Log In" onClick={() => this.login()}/>
                </div>
            </div>
    }
}

export default Login;