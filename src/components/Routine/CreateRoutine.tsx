import React from 'react'
import { Redirect } from 'react-router-dom';
import RoutineForm from './RoutineForm';

interface RoutineState {
    redirect: boolean
}

class CreateRoutine extends React.Component<any, RoutineState> {
    constructor(props:any) {
        super(props);
        this.state = {
            redirect: false
        }
    }

    redirect() {
        this.setState({redirect: true});
    }

    render() {
        const { redirect } = this.state;
        
        return redirect 
            ? <Redirect to="/home" />
            : <RoutineForm callback={() => this.redirect()} />
    }
}

export default CreateRoutine