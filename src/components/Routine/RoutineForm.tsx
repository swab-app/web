import React from 'react';
import { Routine, Exercise, RoutineExercise, SetModel } from '../../models';
import { Button, ButtonRow, FlexBetweenRow, FormRow, H2 } from '../../styled_elements';
import { RoutineService } from '../../services';
import { RoutineRequest } from '../../requests';
import { ExerciseSearch } from '../Exercise';
import { RoutineExercise as RoutineExerciseComponent } from '../RoutineExercise';

// I want something like
// <RoutineForm id?={routine.id} saveCallback={() => this.saveAndRedirect()} />
// doesn't quite work yet.

interface Props {
    id?: number,
    callback: () => void 
}

interface State {
    name: string,
    exercises: Exercise[],
}

class RoutineForm extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            name: '',
            exercises: [],
        }
    }

    componentDidMount() {
        const { id } = this.props;
        if (id !== null && id !== undefined) {
            RoutineService.getRoutineById(id!)
                .then((response) => {
                    this.setState(this.transformRoutineIntoState(response.data));
                })
                .catch(err => {
                    console.error(err);
                });
        }
    }

    create() {
        RoutineService.create(this.transformStateIntoRoutineRequest(), this.props.callback);
    }

    update() {
        RoutineService.update(this.props.id!, this.transformStateIntoRoutineRequest() , this.props.callback);
    }

    transformRoutineIntoState(r: Routine) : State {
        return {
            name: r.name,
            exercises: r.routineExercises.map((re:RoutineExercise) => {
                let ex: Exercise = re.exercise
                ex.sets = re.sets;
                return ex;
            })
        };
    }

    transformStateIntoRoutineRequest() : RoutineRequest {
        return {
            name: this.state.name,
            exercises: this.state.exercises.map((ex: Exercise) => {
                return {
                    id: ex.id,
                    sets: ex.sets?.map((set:SetModel) => {
                        return {
                            weight: set.weight,
                            time: set.time,
                            reps: set.reps,
                            type: set.type,
                            distance: set.distance
                        }
                    }) ?? []
                }
            })
        };
    }


    addExercise(ex:Exercise) {
        let {exercises} = this.state;
        exercises.push({
            id: ex.id,
            name: ex.name,
            type: ex.type,
            sets: ex.sets,
        });
        this.setState({exercises: exercises});
    }

    removeExercise(index:number) {
        let {exercises} = this.state;
        exercises.splice(index, 1);
        this.setState({exercises: exercises});
    }

    updateExerciseSets(index:number, sets:SetModel[]) {
        let {exercises} = this.state;
        exercises[index].sets = sets;
        this.setState({exercises: exercises});
    }

    renderExerciseList() {
        // ToDo: replace this with a 'RoutineExerciseList.
        //       may be a PITA because I'm passing callbacks down the chain. 
        return this.state.exercises.length === 0
            ? <p>No Exercises Selected</p>
            : this.state.exercises.map((ex:Exercise, index:number) => {
                return <RoutineExerciseComponent
                    key={`${ex.name}-${index}`}
                    exercise={ex}
                    removeCallback={() => this.removeExercise(index)}
                    updateCallback={(sets:SetModel[]) => this.updateExerciseSets(index, sets)}
                    />
            });
    }

    render() {
        const isCreate = this.props.id === null || this.props.id === undefined;
        const verb = isCreate ? "Create" : "Update";
        const { name } = this.state;
        const exerciseList = this.renderExerciseList()
        return <div>
            <H2>{verb} Routine</H2>
            <FormRow>
                <label htmlFor="name">Routine Name:</label>
                <input type="text" name="name" value={name} onChange={e => this.setState({name: e.target.value})} />
            </FormRow>
            <FlexBetweenRow>
                <ExerciseSearch selectedCallback={(ex:Exercise) => this.addExercise(ex)} />
                <div style={{minWidth: "50%"}}>
                    {exerciseList}
                </div>
            </FlexBetweenRow>
            <ButtonRow>
                <Button 
                    onClick={() => isCreate ? this.create() : this.update()}>
                        {verb}
                    </Button>
            </ButtonRow>
        </div>
    }   
}

export default RoutineForm;