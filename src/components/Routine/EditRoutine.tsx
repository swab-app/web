import React from 'react';
import { RouteComponentProps, Redirect } from 'react-router';
import RoutineForm from './RoutineForm';

interface RouteParameters {
    id: string,
}

interface EditRoutineProps extends RouteComponentProps<RouteParameters> {}

interface EditRoutineState {
    redirect: boolean,
}

class EditRoutine extends React.Component<EditRoutineProps, EditRoutineState> {
    constructor(props:any) {
        super(props);
        this.state = {
            redirect: false
        }
    }

    redirect() {
        this.setState({redirect: true});
    }

    render() {
        const { redirect } = this.state;
        
        return redirect 
            ? <Redirect to="/home" />
            : <RoutineForm id={Number.parseInt(this.props.match.params.id)} callback={() => this.redirect()} />
    }
}

export default EditRoutine;