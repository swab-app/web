import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { User } from '../models';
import Authenticator from './Authenticator';

const http:AxiosInstance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    headers: {
        'Content-Type': 'application/json'
    }
});
  
http.interceptors.request.use((config:AxiosRequestConfig) => {
    const user: User = Authenticator.getUser();
    if (user.token !== null || user.token !== '') {
        config.headers.Authorization = `Bearer ${user.token}`
    }
    return config;
});

export default http;