import { Authenticator } from ".";
import { User } from '../models';


// Mocking localstorage
class LocalStorageMock {
    constructor() {
        this.store = {};
    }

    clear() {
        this.store = {};
    }

    getItem(key) {
        return this.store[key] || null;
    }

    setItem(key, value) {
        this.store[key] = String(value);
    }

    removeItem(key) {
        delete this.store[key];
    }
}
    
// 'injecting' that mock
global.localStorage = new LocalStorageMock;

// tests
function getUser() : User {
    return {
        id: 1,
        username: 'spkvn',
        email: 'kevin@lol.com',
        token: 'abc123',
    }
}

test('When passed valid user, store in localstorage.', () => {
    // arrange
    const user:User = getUser();
    // act
    Authenticator.setUser(user);
    // assert
    expect(global.localStorage.getItem("swab-token-key")).toEqual(JSON.stringify(user))
});

test('When user stored, return user.', () => {
    // arrange 
    const user:User = getUser();
    Authenticator.setUser(user);
    // act 
    const retrievedUser:User = Authenticator.getUser();
    // assert
    expect(retrievedUser).toEqual(user);
});

test('When logged in user with token stored, return true', () => {
    Authenticator.setUser(getUser());

    const isLoggedIn = Authenticator.isLoggedIn();

    expect(isLoggedIn).toBeTruthy();
});

test('When not logged in, return false', () => {
    Authenticator.removeUser();

    const isLoggedIn = Authenticator.isLoggedIn();

    expect(isLoggedIn).toBeFalsy();
});