import {User} from '../models'

class Authenticator {
    static readonly TOKEN_KEY: string = "swab-token-key";

    static setUser(user:User):void {
        console.log(`setting user with id of ${user.id}`)
        localStorage.setItem(this.TOKEN_KEY, JSON.stringify(user));
    }

    static getUser(): User {
        if (this.isLoggedIn()) {
            let userString = localStorage.getItem(this.TOKEN_KEY) ?? '{}',
                user: User = JSON.parse(userString);
            return user;
        }
        return {} as User;
    }

    static isLoggedIn(): boolean {
        let user:string | null = localStorage.getItem(this.TOKEN_KEY)
        return user !== null && user !== undefined && user !== '';
    }

    static isNotLoggedIn(): boolean {
        return !this.isLoggedIn();
    }

    static removeUser(): void {
        localStorage.removeItem(this.TOKEN_KEY);
    }
}

export default Authenticator;