import React from "react";
import {User} from '../models'

const AuthUserContext = React.createContext<User|null>(null);

export default AuthUserContext;