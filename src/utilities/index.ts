import Authenticator from './Authenticator';
import AuthUserContext from './AuthUserContext';
import http from './http';

function randomString(length:number) {
    return  Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, length); 
}

function isNullOrUndefined(val: any) {
    return val === undefined || val === null;
}

function isNotNullOrUndefined(val: any) {
    return !isNullOrUndefined(val);
}

export {Authenticator, AuthUserContext, http, randomString, isNullOrUndefined, isNotNullOrUndefined};